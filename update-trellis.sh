#!/bin/bash

# SPDX-FileCopyrightText: 2024 Florian Kohrt
#
# SPDX-License-Identifier: CC0-1.0

# Update Script for Trellis
#   ./update-trellis.sh common v1.14.0
#   ./update-trellis.sh custom 74b4f5e --stat
#
# List of custom files from https://discourse.roots.io/t/lets-encrypt-certificate-expired/7171/23

source TRELLIS_COMMON_VERSION
source TRELLIS_CUSTOM_VERSION
if [ -z "$COMMON" ] || \
   [ -z "$CUSTOM" ] || \
   [ -z "$1" ] || \
   [ -z "$2" ]
then
  exit 1
fi

if [ "$1" = "custom" ]
then
  curl -fsSL -o- https://github.com/roots/trellis/compare/"$CUSTOM".."$2".diff \
    | git apply --verbose -p1 --directory='trellis' \
                --include='trellis/ansible.cfg' \
                --include='trellis/group_vars/*' \
                --include='trellis/deploy-hooks/*' \
                --include='trellis/hosts/*' \
                --include='trellis/nginx-includes/*' \
                $3 -
  RESULT=$?
  if [ $RESULT -eq 0 ] && \
     [ "$3" != "--stat" ] && \
     [ "$3" != "--numstat" ] && \
     [ "$3" != "--summary" ] && \
     [ "$3" != "--check" ]
  then
    echo 'CUSTOM='"$2" > TRELLIS_CUSTOM_VERSION
  fi
else
  curl -fsSL -o- https://github.com/roots/trellis/compare/"$COMMON".."$2".diff \
    | git apply --verbose -p1 --directory='trellis' \
                --exclude='trellis/ansible.cfg' \
                --exclude='trellis/group_vars/*' \
                --exclude='trellis/deploy-hooks/*' \
                --exclude='trellis/hosts/*' \
                --exclude='trellis/nginx-includes/*' \
                --exclude='trellis/.vault_pass' \
                --exclude='trellis/.github/*' \
                $3 -
  RESULT=$?
  if [ $RESULT -eq 0 ] && \
     [ "$3" != "--stat" ] && \
     [ "$3" != "--numstat" ] && \
     [ "$3" != "--summary" ] && \
     [ "$3" != "--check" ]
  then
    echo 'COMMON='"$2" > TRELLIS_COMMON_VERSION
  fi
fi

exit $RESULT

