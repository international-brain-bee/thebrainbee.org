<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Load child theme setup first.
require_once trailingslashit( get_stylesheet_directory() ) . 'setup.php';

/**
 * IMPORTANT:
 * Do not remove the code above!
 * Put your custom PHP code below.
 *
 * Child theme functionality:
 */

/**
 * Enqueuing stylesheet file to editor.
 *
 * Remove this when not needed.
 */
add_action( 'init', function() {

	// Add `style.css` file to editor.
	add_editor_style(
		esc_url_raw(
			add_query_arg(
				'ver',
				'v' . WebManDesign\Child_Theme\Setup::$stylesheet_filemtime,
				get_stylesheet_uri()
			)
		)
	);
}, 99 );

/**
 * Allow numeric slugs for child pages
 * 
 * Source: https://wordpress.stackexchange.com/a/405816
 *  */

add_filter( 'pre_wp_unique_post_slug', 'my_pre_wp_unique_post_slug', 10, 6 );
function my_pre_wp_unique_post_slug( $override_slug, $slug, $post_ID, $post_status, $post_type, $post_parent ) {
    $allowed_post_status = array( 'publish', 'private' ); // allows numerical slug only for these statuses

    if ( preg_match( '/^\d{4}$/', $slug ) && $post_parent &&
        'page' === $post_type && in_array( $post_status, $allowed_post_status )
    ) {
        return my_unique_page_slug( $slug, $post_ID, $post_type );
    }

    return $override_slug;
}

// This function is based on the code in the wp_unique_post_slug() function, lines
// 5023-5054 in WordPress v5.9.3, except that $check_sql doesn't include the `AND post_parent = %d`,
// which means we only want unique slug for each Page (i.e. post of type 'page').
function my_unique_page_slug( $slug, $post_ID, $post_type = 'page' ) {
    global $wpdb;

    $check_sql       = "SELECT post_name FROM $wpdb->posts WHERE post_name = %s AND post_type IN ( %s, 'attachment' ) AND ID != %d LIMIT 1";
    $post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $slug, $post_type, $post_ID ) );

    if ( $post_name_check ) {
        $suffix = 2;
        do {
            $alt_post_name   = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) . "-$suffix";
            $post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $alt_post_name, $post_type, $post_ID ) );
            $suffix++;
        } while ( $post_name_check );
        $slug = $alt_post_name;
    }

    return $slug;
}

add_filter( 'page_rewrite_rules', 'my_page_rewrite_rules' );
function my_page_rewrite_rules( $page_rewrite ) {
    return array(
        '^(.+/\d{4})(?:/(\d+))?/?$' => 'index.php?pagename=$matches[1]&page=$matches[2]',
    ) + $page_rewrite;
}
